import json

from motw_client import (
    add_library,
    bookids,
    calibre_to_json,
    library_on,
    load_books,
    remove_books,
    upload_books,
)

from motw_secrets import libraries

HOST_API = "https://library.memoryoftheworld.org"
HOST_API_UPLOAD = "https://upload.memoryoftheworld.org"
# HOST_API = "http://0.0.0.0:2018"
# HOST_API_UPLOAD = "http://0.0.0.0:2020"
#HOST_API = "167.99.173.113:2018"
#HOST_API_UPLOAD = "167.99.173.113:2019"

DB_PATH = "/home/m/CalibreLibraries/"


def add_library_and_books(librarian, lsb_url, db_path, library_uuid, library_secret):
    print(f">>>  {librarian} from {lsb_url}")
    try:
        add = add_library(HOST_API, library_uuid, library_secret)
        if add[0] == 200:
            print(f"{librarian} from {lsb_url} added to {HOST_API}")
        elif add[0] == 202:
            print(add[1].decode())
    except Exception as e:
        print("add_library failed: {}".format(e))

    bids = bookids(HOST_API, library_uuid, library_secret)
    if bids[0] == 200:
        bids = json.loads(bids[1].decode())
    else:
        raise Exception(f"Request to {HOST_API} for book ids of {library_uuid} failed.")
    books_list = calibre_to_json(library_uuid, library_secret, librarian, db_path)
    rids = set(
        [
            s.split("___")[0]
            for s in list(
                set(bids)
                - set(
                    ["{}___{}".format(b["_id"], b["last_modified"]) for b in books_list]
                )
            )
        ]
    )
    if len(rids) > 0:
        print("{} books to remove.".format(len(rids)))
    if rids:
        reset = remove_books(
            HOST_API, library_uuid, library_secret, json.dumps(list(rids)), True
        )
        if reset[0] == 200:
            reset = json.loads(reset[1].decode())
        else:
            raise Exception(
                f"Request to {HOST_API} to remove books from {library_uuid} failed."
            )

        if reset["reset"]:
            # library_off(library_uuid, library_secret)
            l = library_on(
                HOST_API, library_uuid, library_secret, librarian, lsb_url, "on"
            )
            if l[0] == 200:
                print(f"{librarian}'s {len(rids)} books removed and it is back online.")
        else:
            l = library_on(HOST_API, library_uuid, library_secret, librarian, lsb_url)
            if l[0] == 202:
                print(f"{librarian}'s {len(rids)} books removed and it is back online.")

    bids = bookids(HOST_API, library_uuid, library_secret)
    if bids[0] == 200:
        bids = json.loads(bids[1].decode())
    else:
        raise Exception(f"Request to {HOST_API} for book ids of {library_uuid} failed.")
    sids = set(
        [
            s.split("___")[0]
            for s in list(
                set(
                    ["{}___{}".format(b["_id"], b["last_modified"]) for b in books_list]
                )
                - set(bids)
            )
        ]
    )

    if len(sids) > 0:
        print("{} books to upload...".format(len(sids)), end="")

    if sids:
        # reset = add_books(
        upload = upload_books(
            # HOST_API,
            HOST_API_UPLOAD,
            library_uuid,
            library_secret,
            json.dumps([b for b in books_list if b["_id"] in sids]),
            True,
        )
        if upload[0] == 200:
            reset = load_books(HOST_API, "add", library_uuid, library_secret)
        if reset[0] == 200:
            reset = json.loads(reset[1].decode())
            print(" Uploaded.")
        else:
            raise Exception(
                f"Request to load books of just uploaded {library_uuid} failed."
            )

        if reset["reset"]:
            # library_off(library_uuid, library_secret)
            l = library_on(
                HOST_API, library_uuid, library_secret, librarian, lsb_url, "on"
            )
            if l[0] == 200:
                print(f"{librarian} from {lsb_url} is back online.")
        else:
            l = library_on(HOST_API, library_uuid, library_secret, librarian, lsb_url)
            if l[0] == 200:
                print(f"{librarian} from {lsb_url} is back online.")

    print("")


for library in libraries:
    # print(library[0])
    librarian = library[3]
    lsb_url = "//{}.memoryoftheworld.org/".format(library[0])
    db_path = "{}{}/metadata.db".format(DB_PATH, library[0])
    library_uuid = library[1]
    library_secret = library[2]
    add_library_and_books(librarian, lsb_url, db_path, library_uuid, library_secret)
