import { writable } from 'svelte/store';
import { readable } from 'svelte/store';

export const lasthash = writable("[\"\",\"\"]");
// export const restapi = readable("http://localhost:2018/");
export const restapi = readable("https://books.memoryoftheworld.org/");
