module.exports = {
    theme: {
        screens: {
            'monocle': {'max': '599px'},
            'phone': {'min': '600px',
                      'max': '767px'},
            'phablet': {'min': '768px'},
            'tablet': {'min': '1024px'},
            'laptop': {'min': '1280px'},
            'suv': {'min': '2048px'}
        },
        fontFamily: {
            'motw': ['IBMPlexMono', 'Courier New','monospace']
        },
        extend: {
            colors: {
                'motw-red': '#ff0000',
                'motw-blue': '#007bff'
            },
        }
    },
    variants: {
        backgroundColor: ['responsive', 'hover', 'focus', 'active'],
        textColor: ['responsive', 'hover', 'focus', 'active'],
        userSelect: ['responsive', 'hover', 'focus', 'active']
    },
    plugins: []
}
