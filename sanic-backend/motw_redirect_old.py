from sanic import Sanic
from sanic.response import json
import pickle
import sys
from setproctitle import setproctitle

if len(sys.argv) > 2:
    setproctitle(sys.argv[2])
else:
    setproctitle("motw_redirect")


app = Sanic()

on = pickle.loads(open("motw_old.pickle", "rb").read())


@app.route("/b/<book_id>")
async def test(request, book_id):
    new_id = on.get(book_id)
    if new_id:
      if "id" in new_id.keys():
        book_id = new_id['id']
        return json(
            {"message": "moved!"},
            headers={
                "Location": f"https://library.memoryoftheworld.org/#/book/{book_id}"
            },
            status=301,
        )
      elif "title" in new_id.keys():
        book_title = new_id['title']
        return json(
            {"message": "moved!"},
            headers={
                "Location": f"https://library.memoryoftheworld.org/#/search/title/{book_title}"
            },
            status=301,
        )
    else:
        return json(
            {"message": "moved!"},
            headers={"Location": "https://library.memoryoftheworld.org/"},
            status=301,
        )


app.run(host="0.0.0.0", port=int(sys.argv[1]), workers=1, debug=False, access_log=False)
