import glob
import pathlib
import pickle
import os
import re
import shortuuid
import sys
import time
import urllib
import zlib
from unidecode import unidecode
from urllib.parse import unquote_plus as unq
from xml.sax.saxutils import escape

import rapidjson as rjson
from sanic import Sanic
from sanic import response as rs
from sanic.exceptions import SanicException, abort
from sanic.response import json, text
from sanic.views import HTTPMethodView
from sanic.views import stream as stream_decorator

from sanic.log import logger
from sanic_cors import CORS
from setproctitle import setproctitle

import motw

if len(sys.argv) > 2:
    setproctitle(sys.argv[2])
else:
    setproctitle("motw_api")

shortuuid.set_alphabet("23456789abcdefghijkmnopqrstuvwxyz")

MOTW_LOG_PATH = os.environ.get("MOTW_LOG_PATH", "/root/motw_logs/webapp.logs")
LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "format": '{"timestamp": "%(asctime)s", %(message)s}',
            "datefmt": "%Y-%m-%d %H:%M:%S",
            "class": "logging.Formatter",
        },
    },
    "loggers": {
        "": {"level": "WARNING", "handlers": ["motw"]},
        "sanic.root": {"level": "INFO", "handlers": ["default"]},
        "sanic.error": {"level": "INFO", "handlers": ["default"]},
        "sanic.access": {"level": "INFO", "propagate": True, "handlers": ["default"]},
    },
    "handlers": {
        "motw": {
            "class": "logging.FileHandler",
            "filename": MOTW_LOG_PATH,
            "formatter": "standard",
            "level": "WARNING",
            "mode": "w",
        },
        "default": {
            "level": "INFO",
            "formatter": "standard",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stdout",
        },
    },
}

app = Sanic("motw_api", log_config=LOGGING_CONFIG)

# Compress(app)
GTFOH = "change_it"

CORS(app)
app.config.REQUEST_MAX_SIZE = 1_000_000_000
app.config.PROXIES_COUNT = 1


def clear_cache():
    motw.books_cache.clear()
    for fpath in glob.glob("unitedstates/search_cache/*"):
        pathlib.Path(fpath).unlink()
    motw.cache_id = shortuuid.uuid()[:12]


def update_motw_books(library_uuid):
    with open(f"unitedstates/{library_uuid}", "rb") as f:
        motw.books.update(pickle.load(f))


def update_motw_indexes():
    for b in motw.books.values():
        motw.indexed_by_time.update({str(b["last_modified"]) + b["_id"]: b["_id"]})
        # motw.indexed_by_title.update({b["title_sort"] + b["_id"]: b["_id"]})
        # motw.indexed_by_pubdate.update({str(b["pubdate"]) + b["_id"]: b["_id"]})


def remove_book_from_indexes(bookid, book):
    motw.indexed_by_time.pop(f"{book['last_modified']}{bookid}", None)
    # motw.indexed_by_title.pop(f"{book['title_sort']}{bookid}", None)
    # motw.indexed_by_pubdate.pop(f"{book['pubdate']}{bookid}", None)


def empty_pickle(library_uuid):
    with open(f"unitedstates/{library_uuid}", "wb") as f:
        pickle.dump({}, f)


def pickle_books(library_uuid, pickled_books):
    with open(f"unitedstates/{library_uuid}", "wb") as f:
        pickle.dump(pickled_books, f)


def update_pickled_books(library_uuid):
    with (open(f"unitedstates/{library_uuid}", "rb")) as f:
        return pickle.load(f)


# - initial (re)load
if not pathlib.Path("unitedstates/search_cache").exists():
    pathlib.Path("unitedstates/search_cache").mkdir(parents=True, exist_ok=True)

motw.libraries = motw.load_libraries()

for library_uuid_ in motw.libraries:
    t = time.time()
    if motw.libraries[library_uuid_]["state"] == "on":
        if not pathlib.Path(f"unitedstates/{library_uuid_}").exists():
            empty_pickle(library_uuid_)
            motw.libraries[library_uuid_]["state"] = "off"
            motw.dump_libraries(motw.libraries)
            continue

        update_motw_books(library_uuid_)

    logger.warning(
        f'"info": "initial library update","library_url": "{motw.libraries[library_uuid_]["url"]}", "timespan": "{round(time.time() - t, 3)}"'
    )

t = time.time()
clear_cache()
update_motw_indexes()
logger.warning(
    f'"info": "initial index update", "timespan": "{round(time.time() - t, 3)}"'
)


# - SANIC/rapidjson
def add_library_url(book):
    library = motw.libraries[book["library_uuid"]]
    book.update({"library_url": library["url"], "librarian": library["librarian"]})
    return book


def get_max_results(request):
    max_results = motw.hateoas.max_results
    if "max_results" in request.args:
        try:
            max_results = min(120, abs(int(request.args["max_results"][0])))
        except Exception as exc:
            logger.info("messy max_results was sent: {}".format(exc))
    return max_results


def get_page(request, length, max_results):
    last_page = int(length / max_results + 1)
    if "page" in request.args:
        page = min(last_page, max(0, int(request.args["page"][0]) - 1))
    else:
        page = 0
    return page, last_page


def get_cache_fingerprint(request):
    cf = ""
    for p in request.path.split("/"):
        if p:
            cf += f"_{p}_"
    return cf


def check_cache(request):
    t = time.time()
    cache_fingerprint = get_cache_fingerprint(request)
    for c in motw.books_cache:
        if cache_fingerprint in c:
            logger.warning(
                f'"info":"check validation", "user_ip": "{request.remote_addr}", "request_path": {rjson.dumps(request.path)}, "timespan": "{round(time.time() - t, 3)}", "request_string": "{request.query_string}"'
            )
            return c[cache_fingerprint]
    return False


def hateoas(length, request, title="books"):
    max_results = get_max_results(request)
    page, last_page = get_page(request, length.__len__(), max_results)

    result = {}
    books = [
        {
            k: v
            for (k, v) in book.items()
            if k
            in [
                "_id",
                "authors",
                "pubdate",
                "title",
                "formats",
                "library_uuid",
                "cover_url",
            ]
        }
        for book in length[page * max_results : (page + 1) * max_results]
    ]
    result = {
        "_items": [add_library_url(book) for book in books],
        "_links": {
            "parent": {"title": "Memory of the World Library", "href": "/"},
            "self": {"title": title, "href": request.path[1:]},
            "prev": {"title": "previous page", "href": f"{request.path}?page={page}"},
            "next": {"title": "next page", "href": f"{request.path}?page={page+2}"},
            "last": {"title": "last page", "href": f"{request.path}?page={last_page}"},
        },
        "_meta": {
            "page": page + 1,
            "max_results": max_results,
            "total": len(length),
            "status": title,
        },
    }

    if page == 1:
        result["_links"]["prev"]["href"] = f"{request.path}"
    if page == 0:
        del result["_links"]["prev"]
    if page == last_page - 1:
        del result["_links"]["next"]

    res = rjson.dumps(result, datetime_mode=rjson.DM_ISO8601).encode()
    return res


def check_library_secret(library_uuid, library_secret):
    secret = motw.load_libraries()[library_uuid]["secret"]
    ret = (
        True
        if (library_secret == motw.master_secret or library_secret == secret)
        else False
    )
    if ret:
        return ret
    else:
        abort(403)


async def validate_books(bookson, schema, enc_zlib):
    if enc_zlib:
        try:
            bookson = zlib.decompress(bookson).decode("utf-8")
        except zlib.error as exc:
            abort(422, f"Unzipping JSON failed: {exc}")

    validate = rjson.Validator(rjson.dumps(schema))
    try:
        validate(bookson)
        return bookson
    except ValueError as exc:
        logger.warning(f'"info": "JSON with books validation failed: {exc}"')
        abort(422, "JSON didn't validate.")


async def remove_books(rookson, library_uuid, load=False):
    reset = False
    if load:
        bookids = rookson
    else:
        bookids = rjson.loads(rookson)
    if bookids == []:
        return True, reset

    pickled_books = update_pickled_books(library_uuid)

    for bookid in bookids:
        if motw.libraries[library_uuid]["state"] == "on":
            book = motw.books[bookid]
            remove_book_from_indexes(bookid, book)

            del motw.books[bookid]
        pickled_books.pop(bookid, None)

    if motw.libraries[library_uuid]["state"] == "on":
        clear_cache()

    pickle_books(library_uuid, pickled_books)

    return True, reset


# @sync_to_async
async def add_books(bookson, library_uuid, load=False):
    reset = False
    t = time.time()
    if load:
        books = bookson
        logger.warning(
            f'"info": "{motw.libraries[library_uuid]["url"]} books.json loaded", "timespan": "{round(time.time() -t, 3)}"'
        )
    else:
        books = rjson.loads(bookson, datetime_mode=rjson.DM_ISO8601)
        logger.warning(
            f'"info": "{motw.libraries[library_uuid]["url"]} books.json loaded", "timespan": "{round(time.time() -t, 3)}"'
        )

    if books == []:
        return True, reset

    library_uuid_check = list(set([book["library_uuid"] for book in books]))
    if len(library_uuid_check) != 1 or library_uuid_check[0] != library_uuid:
        return False, reset

    new_books_ids = set((book["_id"] for book in books))
    t = time.time()

    if not pathlib.Path(f"unitedstates/{library_uuid}").exists():
        empty_pickle(library_uuid)

    pickled_books = update_pickled_books(library_uuid)
    old_books_ids = pickled_books.keys()
    logger.warning(
        f'"info": "{motw.libraries[library_uuid]["url"]} pickle loaded", "timespan": "{round(time.time() -t, 3)}"'
    )

    t = time.time()
    ids_to_add = set(new_books_ids - old_books_ids)
    for book in books:
        if book["_id"] not in ids_to_add:
            continue

        if not reset:
            reset = True

        if "series" not in book:
            book["series"] = ""

        pickled_books.update({book["_id"]: book})

    logger.warning(
        f'"info": "{motw.libraries[library_uuid]["url"]} pickle updated", "timespan": "{round(time.time() -t, 3)}"'
    )

    t = time.time()
    pickle_books(library_uuid, pickled_books)

    logger.warning(
        f'"info": "{motw.libraries[library_uuid]["url"]} pickle dumpped", "timespan": "{round(time.time() -t, 3)}"'
    )
    return True, reset


class Books(HTTPMethodView):
    def get(self, request, verb, library_uuid):
        assert request.stream is None
        return text("Here you should upload, no?")

    @stream_decorator
    async def post(self, request, verb, library_uuid):
        t = time.time()
        library_secret = request.headers.get("Library-Secret") or request.headers.get(
            "library-secret"
        )

        check_library_secret(library_uuid, library_secret)

        encoding_header = request.headers.get(
            "library-encoding"
        ) or request.headers.get("Library-Encoding")
        enc_zlib = False
        if encoding_header == "zlib":
            enc_zlib = True
        result = b""
        while True:
            body = await request.stream.read()
            if body is None:
                logger.warning(
                    f'"info": "{motw.libraries[library_uuid]["url"]} books uploaded", "timespan": "{round(time.time() -t, 3)}"'
                )
                if verb == "add":
                    t = time.time()
                    bookson = await validate_books(
                        result, motw.collection_schema, enc_zlib
                    )
                    logger.warning(
                        f'"info": "{motw.libraries[library_uuid]["url"]} books validated", "timespan": "{round(time.time() -t, 3)}"'
                    )
                    if bookson:
                        t = time.time()
                        status, reset = await add_books(bookson, library_uuid)
                        if status:
                            logger.warning(
                                f'"info": "{motw.libraries[library_uuid]["url"]} books added", "timespan": "{round(time.time() -t, 3)}"'
                            )
                            return json({"reset": reset})
                elif verb == "remove":
                    t = time.time()
                    bookson = await validate_books(result, motw.remove_schema, enc_zlib)

                    if bookson:
                        status, reset = await remove_books(bookson, library_uuid)
                        if status:
                            logger.warning(
                                f'"info": "{motw.libraries[library_uuid]["url"]} books removed", "timespan": "{round(time.time() -t, 3)}"'
                            )
                            return json({"reset": reset})

                abort(422, f"{verb}-ing books failed!")
            result += body


@app.route("/library/<verb>/<library_uuid>")
async def library(request, verb, library_uuid):
    library_secret = request.headers.get("Library-Secret") or request.headers.get(
        "library-secret"
    )
    r = re.match(
        "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}",
        library_secret,
    )
    if not r or verb not in ["add", "remove", "bookids", "on", "off"]:
        abort(422, "Wrong verb, ha!")

    if verb == "add":
        if library_uuid not in motw.libraries:
            motw.libraries.update(
                {
                    library_uuid: {
                        "secret": library_secret,
                        "state": "off",
                        "url": "",
                        "librarian": "",
                    }
                }
            )
            empty_pickle(library_uuid)

            motw.dump_libraries(motw.libraries)
            return text(f"{library_uuid} added. Let's share books...")
        else:
            return text(f"{library_uuid} already added.", status=202)

    if not check_library_secret(library_uuid, library_secret):
        abort(401)

    if verb == "remove" and library_uuid in motw.libraries:
        with open(f"unitedstates/{library_uuid}", "rb") as f:
            if len(pickle.load(f)) != 0:
                abort(422, f"{library_uuid} has still some books. Remove them first.")
        del motw.libraries[library_uuid]
        motw.dump_libraries(motw.libraries)
        return text(f"{library_uuid} removed.")

    elif verb == "on" and library_uuid in motw.libraries:
        t = time.time()
        if "librarian" in request.args and request.args["librarian"][0] != "":
            librarian = request.args["librarian"][0]
            for library in motw.libraries.items():
                if library[1]["librarian"] == librarian and library_uuid != library[0]:
                    abort(422, "Library needs an *unique* librarian's name.")
            if motw.libraries[library_uuid]["librarian"] != librarian:
                motw.libraries[library_uuid]["librarian"] = librarian
                motw.libraries[library_uuid]["state"] == "off"

        if motw.libraries[library_uuid]["librarian"] == "":
            abort(422, "Library needs a librarian's name.")

        librarian = motw.libraries[library_uuid]["librarian"]
        url = motw.libraries[library_uuid]["url"]

        if "url" in request.args:
            if request.args["url"][0] != url:
                motw.libraries[library_uuid]["url"] = request.args["url"][0]
            elif (
                motw.libraries[library_uuid]["state"] == "on"
                and request.args["url"][0] == url
                and "reset" not in request.args
            ):
                return text(
                    f"{library_uuid} by {librarian} served at {url} is already online.",
                    status=202,
                )
        if (
            "url" not in request.args
            and "librarian" not in request.args
            and "reset" not in request.args
            and motw.libraries[library_uuid]["state"] == "on"
        ):
            return text(
                f"{library_uuid} by {librarian} served at {url} is already online.",
                status=202,
            )

        update_motw_books(library_uuid)
        update_motw_indexes()

        motw.libraries[library_uuid]["state"] = "on"
        motw.dump_libraries(motw.libraries)

        clear_cache()
        logger.warning(
            f'"info": "library {motw.libraries[library_uuid]["url"]} ON", "timespan": "{round(time.time() -t, 3)}"'
        )
        return text(f"{library_uuid} by {librarian} served at {url} is now online.")

    elif verb == "off" and library_uuid in motw.libraries:
        t = time.time()
        if motw.libraries[library_uuid]["state"] == "off":
            return text(f"{library_uuid} is not online.", status=202)

        bookids = []
        for b in motw.books.values():
            if b["library_uuid"] == library_uuid:
                bookid = b["_id"]
                bookids.append(bookid)
                book = motw.books[bookid]
                remove_book_from_indexes(bookid, book)

        for bid in bookids:
            del motw.books[bid]

        motw.libraries[library_uuid]["state"] = "off"
        motw.dump_libraries(motw.libraries)
        clear_cache()
        logger.warning(
            f'"info": "library {motw.libraries[library_uuid]["url"]} OFF", "timespan": "{round(time.time() -t, 3)}"'
        )
        return text(f"{library_uuid} is now offline.")

    elif verb == "bookids" and library_uuid in motw.libraries:
        bookids = [
            f"{book['_id']}___{book['last_modified']}"
            for book in motw.books.values()
            if library_uuid == book["library_uuid"]
        ]
        if bookids == []:
            try:
                with (open(f"unitedstates/{library_uuid}", "rb")) as f:
                    bookids = [
                        f"{book['_id']}___{book['last_modified']}"
                        for book in pickle.load(f).values()
                    ]
            except Exception as exc:
                logger.warning(f'"info": "No cache. EXCEPTION: {exc}"')
                bookids = []
        return json(bookids)
    elif library_uuid not in motw.libraries:
        abort(404, f"{library_uuid} doesn't exist.")


# @app.route("/memory")
# async def get_memory(request):
#     proc = psutil.Process(os.getpid())
#     import ipdb; ipdb.set_trace()
#     return json(
#         {"memory": "{}".format(round(proc.memory_full_info().rss / 1000000.0, 2))}
#     )


@app.route("/search/<field>/<q>")
async def search(request, field, q):
    async def _download_links(request, field, q, ff):
        # import ipdb; ipdb.set_trace()
        t = time.time()
        cache_fingerprint = ""

        for p in request.path.split("/"):
            if p:
                cache_fingerprint += f"_{p}_"

        dl_base = "unitedstates/search_cache/"
        dl_ext = request.args["dl"][0]
        dl_filename = f"{dl_base}{cache_fingerprint}.{dl_ext}"

        if pathlib.Path(dl_filename).exists():
            return dl_filename

        cache_subdomain = f"00-{motw.cache_id}"
        sq = zlib.crc32(cache_fingerprint.encode())
        metalink_files = []
        wget_urls = []
        books_matched = []
        datajs_path = "/var/www/html/datajs"
        # datajs_path = f"/tmp/datajs"
        datajs_cache_path = f"{datajs_path}/{cache_subdomain}-{sq}"

        pathlib.Path(f"{datajs_cache_path}/static").mkdir(parents=True, exist_ok=True)

        search_query = unidecode(unq(q).lower())
        for b in motw.books.values():
            if len(books_matched) >= 1025:
                break

            if ff:
                field_value = unidecode(b[field].lower())
            else:
                field_value = unidecode(" ".join(b[field]).lower())

            if search_query in field_value:
                books_matched.append(b)
                library_url = add_library_url(b)["library_url"]
                cover = b["cover_url"]
                url = urllib.parse.quote(f"http:{library_url}{cover}").replace(
                    "%3A", ":"
                )
                metalink_files.append(
                    motw.metalink_files(escape(cover), 0, escape(url))
                )
                wget_urls.append(url.encode())

                metadata_url = url.replace("cover.jpg", "metadata.opf")
                metadata_dir = cover.replace("cover.jpg", "metadata.opf")
                metalink_files.append(
                    motw.metalink_files(escape(metadata_dir), 0, escape(metadata_url))
                )
                wget_urls.append(metadata_url.encode())

                for frm in b["formats"]:
                    url = urllib.parse.quote(
                        f"http:{library_url}{frm['dir_path']}{frm['file_name']}"
                    ).replace("%3A", ":")
                    metalink_files.append(
                        motw.metalink_files(
                            escape(f"{frm['dir_path']}{frm['file_name']}"),
                            frm["size"],
                            escape(url),
                        )
                    )
                    wget_urls.append(url.encode())

        # write first 48 books_matched
        count = 1
        block = 24
        with open(f"{datajs_cache_path}/static/data{count}.js", "wb") as f:
            books_json = rjson.dumps(
                {
                    "portable": True,
                    "total": len(books_matched),
                    "books": books_matched[:block],
                },
                datetime_mode=rjson.DM_ISO8601,
            )
            output = f"CALIBRE_BOOKS{count}={books_json}"
            f.write(output.encode())

        # write six more (2x96, 4x2304)
        count += 1
        for i in range(1, 3):
            for ii in range(2 ** i):
                end_block = block + 24 ** i * 4
                with open(f"{datajs_cache_path}/static/data{count}.js", "wb") as f:
                    books_json = rjson.dumps(
                        {"portable": True, "books": books_matched[block:end_block]},
                        datetime_mode=rjson.DM_ISO8601,
                    )
                    output = f"CALIBRE_BOOKS{count}={books_json}"
                    f.write(output.encode())
                block = end_block

                count += 1
        # write last one with the rest
        with open(f"{datajs_cache_path}/static/data{count}.js", "wb") as f:
            books_json = rjson.dumps(
                {"portable": True, "books": books_matched[block:]},
                datetime_mode=rjson.DM_ISO8601,
            )
            output = f"CALIBRE_BOOKS{count}={books_json}"
            f.write(output.encode())

        datajs_files = [
            "static/css/bundle.css",
            "static/css/bundle.css.map",
            "static/js/bundle.js",
            "static/js/bundle.js.map",
            "static/favicons/favicon-32x32.png",
            "static/favicons/android-chrome-192x192.png",
            "static/favicons/mstile-150x150.png",
            "static/favicons/safari-pinned-tab.svg",
            "static/favicons/favicon-16x16.png",
            "static/favicons/apple-touch-icon.png",
            "static/favicons/android-chrome-512x512.png",
            "static/fonts/IBMPlexMono-Italic.woff2",
            "static/fonts/IBMPlexMono-Bold.woff2",
            "static/fonts/IBMPlexMono.woff2",
            "static/fonts/IBMPlexMono-Regular.woff2",
            "BROWSE_LIBRARY.html",
        ]
        for d in datajs_files:
            metalink_files.insert(
                0,
                motw.metalink_files(
                    d,
                    os.stat(f"{datajs_path}/{d}").st_size,
                    f"https://datajs.memoryoftheworld.org/{d}",
                ),
            )
            wget_urls.insert(0, f"https://datajs.memoryoftheworld.org/{d}".encode())

        for i in range(1, 9):
            metalink_files.insert(
                0,
                motw.metalink_files(
                    f"static/data{i}.js",
                    os.stat(f"{datajs_cache_path}/static/data{i}.js").st_size,
                    f"https://dtjs-{cache_subdomain}-{sq}.memoryoftheworld.org/static/data{i}.js",
                ),
            )

            wget_urls.insert(
                0,
                f"https://dtjs-{cache_subdomain}-{sq}.memoryoftheworld.org/static/data{i}.js".encode(),
            )

        res = motw.metalink_header + "\n".join(metalink_files) + motw.metalink_footer
        with open(f"{dl_base}{cache_fingerprint}.meta4", "wb") as f:
            f.write(res.encode())
        with open(f"{dl_base}{cache_fingerprint}.txt", "wb") as f:
            output = b"\n".join(wget_urls)
            print(output[:50])
            f.write(output)
            logger.warning(
                f'"info": "metalink/wget", "user_ip": "{request.remote_addr}", "request_path": "{request.path}", "query_string":"{request.query_string}", "timespan": "{round(time.time() -t, 3)}"'
            )
        return dl_filename

    try:
        search_query = unidecode(unq(q).lower())
        title = "search in {}".format(field)
        if field == "titles":
            field = "title"

        if field == "librarian":
            for library in motw.libraries.items():
                if unq(q).lower() == library[1]["librarian"].lower():
                    field = "library_uuid"
                    search_query = library[0]
            if field == "librarian":
                abort(404, "No librarian for that query.")
        if (
            "dl" in request.args
            and request.args["dl"][0] in ["txt", "meta4"]
            and field
            in [
                "title",
                "publisher",
                "series",
                "library_uuid",
                "abstract",
                "authors",
                "languages",
                "tags",
            ]
        ):
            dl_ext = request.args["dl"][0]
            ff = True
            if field in ["authors", "languages", "tags"]:
                ff = False
            dl_filename = await _download_links(request, field, q, ff)
            if dl_ext == "meta4":
                m_type = "application/metalink4+xml"
            else:
                m_type = "text/plain"

            return await rs.file_stream(
                f"{dl_filename}",
                chunk_size=1024,
                mime_type=f"{m_type}",
                headers={
                    "Content-Disposition": f'Attachment; filename="search_{field}_{search_query}.{dl_ext}"',
                    "Content-Type": m_type,
                },
            )

        t = time.time()
        if field in ["_id", "title", "publisher", "series", "library_uuid", "abstract"]:
            res = check_cache(request)
            if not res:
                res = [
                    motw.books[bid]
                    for bid in motw.indexed_by_time.values()
                    if search_query in unidecode(motw.books[bid][field].lower())
                ][::-1]
                cache_fingerprint = get_cache_fingerprint(request)
                if cache_fingerprint not in motw.books_cache:
                    motw.books_cache.append({cache_fingerprint: res})

                logger.warning(
                    f'"info": "search", "user_ip": "{request.remote_addr}", "request_path": "{request.path}", "query_string":"{request.query_string}", "timespan": "{round(time.time() -t, 3)}"'
                )
            return rs.raw(hateoas(res, request, title), content_type="application/json")

        if field in ["authors", "languages", "tags"]:
            res = check_cache(request)
            if not res:
                res = [
                    motw.books[bid]
                    for bid in motw.indexed_by_time.values()
                    if search_query
                    in unidecode(" ".join(motw.books[bid][field]).lower())
                ][::-1]
                cache_fingerprint = get_cache_fingerprint(request)
                if cache_fingerprint not in motw.books_cache:
                    motw.books_cache.append({cache_fingerprint: res})
                logger.warning(
                    f'"info": "search", "user_ip": "{request.remote_addr}", "request_path": "{request.path}", "timespan": "{round(time.time() -t, 3)}"'
                )
            return rs.raw(hateoas(res, request, title), content_type="application/json")

        if field in [
            "_lodestone",
            "_isbn",
            "_doi",
            "_amazon",
            "_doi",
            "_issn",
            "_google",
            "_lccn",
            "_md5",
            "_sha256",
            "_sha1",
            "_goodreads",
            "_barnesnoble",
            "_mobi-asin",
        ]:
            logger.warning(f'"info": "id:{field[1:]}", "user_ip": "{request.remote_addr}", "request_path": "{request.path}", "query_string": "{request.query_string}", "timespan": "{round(time.time() -t, 3)}"')
            h = hateoas(
                [
                    book
                    for book in motw.books.values()
                    if {"scheme": field[1:], "code": str(q)} in book["identifiers"]
                ],
                request,
                title,
            )
            return rs.raw(h, content_type="application/json")

    except Exception as exc:
        abort(404, exc)


@app.route("/autocomplete/<field>/<sq>")
async def autocomplete(request, field, sq):
    search_query = unidecode(unq(sq).lower())
    r = set()
    if len(sq) < 4:
        return json({})
    if field in ["titles", "publisher"]:
        if field == "titles":
            field = "title"
            if search_query in ["the "]:
                return json({})
        r = [
            b[field]
            for b in motw.books.values()
            if search_query in unidecode(b[field].lower())
        ]
    elif field in ["authors", "tags"]:
        for bf in [b[field] for b in motw.books.values()]:
            for f in bf:
                if search_query in unidecode(f.lower()):
                    r.add(f)
    r = {"_items": list(r)}
    return json(r)


@app.route("/books/load/<verb>/<library_uuid>")
async def load_books(request, verb, library_uuid):
    library_secret = request.headers.get("Library-Secret") or request.headers.get(
        "library-secret"
    )

    check_library_secret(library_uuid, library_secret)

    t = time.time()

    pname = f"/tmp/{library_uuid}_{library_secret}.pickle"
    with open(pname, "rb") as f:
        bookson = pickle.load(f)
        pathlib.Path(pname).unlink()

    if bookson:
        if verb == "add":
            status, reset = await add_books(bookson, library_uuid, True)
            if status:
                logger.warning(
                    f'"info": "loading library pickle (add)", "timespan": "{round(time.time() -t, 3)}"'
                )
                return json({"reset": reset})
        elif verb == "remove":
            status, reset = await remove_books(bookson, library_uuid, True)
            if status:
                logger.warning(
                    f'"info": "loading library pickle (remove)", "timespan": "{round(time.time() -t, 3)}"'
                )
                return json({"reset": reset})

    abort(422, "Loading uploaded file failed...")


@app.route("/books")
async def books(request):
    t = time.time()
    res = check_cache(request)
    if not res:
        res = [motw.books[bid] for bid in motw.indexed_by_time.values()][::-1]
        cache_fingerprint = get_cache_fingerprint(request)
        if cache_fingerprint not in motw.books_cache:
            motw.books_cache.append({cache_fingerprint: res})
        logger.warning(
            f'"info": "front page", "user_ip": "{request.remote_addr}", "query_string": "{request.query_string}", "request_path": "{request.path}", "timespan": "{round(time.time() -t, 3)}"'
        )
    return rs.raw(hateoas(res, request), content_type="application/json")


@app.route("/book/<book_id>")
async def book(request, book_id):
    try:
        authors = ""
        for author in motw.books[book_id]["authors"]:
            authors += f"""{author.replace("'", "’").replace('"', "")}, """
        authors = authors[:-2]
        title = motw.books[book_id]["title"].replace('"', "")
        logger.warning(
            f'''"info": "single book", "user_ip": "{request.remote_addr}", "query_string": "{request.query_string}", "request_path": "{request.path}", "book_title": "{title}", "book_authors": "{authors}"'''
        )
        return rs.raw(
            rjson.dumps(
                add_library_url(motw.books[book_id]), datetime_mode=rjson.DM_ISO8601
            ).encode(),
            content_type="application/json",
        )
    except Exception as exc:
        abort(404, exc)


@app.route("/librarians/<state>")
async def librarians(request, state):
    librarians = []
    for library in motw.libraries.values():
        if library["state"] == state:
            librarians.append(library["librarian"])

    result = {"_items": librarians}
    return json(result)


@app.route("fuckers/<gtfoh>")
async def fuckers(request, gtfoh):
    if gtfoh == GTFOH:
        with open("/tmp/fuckers", "rb") as f:
            motw.fuckers = pickle.load(f)
            return text("Get the fuck out of here!")
    else:
        return text("Forgot password?")

@app.middleware("request")
async def gtfoh(request):
    if request.remote_addr in motw.fuckers:
        logger.warning(f'"info": "GTFOH FUCKER!", "user_ip": "{request.remote_addr}", "request_path": "{request.path}", "query_string": "{request.query_string}"')
        return rs.redirect("https://download.microsoft.com/download/0/b/c/0bce9bdd-2c6e-43fc-b236-045b6359b9cf/WinDev2007Eval.VMware.zip")


app.add_route(Books.as_view(), "/books/<verb>/<library_uuid>")

# asyncio.set_event_loop(uvloop.new_event_loop())
# server = app.create_server(host="0.0.0.0", port=sys.argv[1])
# loop = asyncio.get_event_loop()
# task = asyncio.ensure_future(server)

# signal(SIGINT, lambda s, f: loop.stop())

# try:
#     loop.run_forever()
# except:
#     loop.stop()


@app.exception(SanicException)
async def throw_away_exceptions(request, exception):
    return text(f"{exception.status_code} {exception}", status=exception.status_code)


app.static("/", "../svelte-frontend/public/index.html")
app.static("/static/", "../svelte-frontend/public/static/")
app.run(host="0.0.0.0", port=int(sys.argv[1]), workers=1, debug=False, access_log=True)
# app.run(host="0.0.0.0", port=int(sys.argv[1]) )
# app.run(host="0.0.0.0")
