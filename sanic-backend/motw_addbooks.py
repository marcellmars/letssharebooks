import rapidjson as rjson
import ujson
import psutil
import os
import gc
import sys
import time
import re
import zlib
import pickle
import pathlib
from signal import signal, SIGINT
from asgiref.sync import sync_to_async
import sortedcontainers
from setproctitle import setproctitle
from urllib.parse import unquote_plus as unq

from sanic import Sanic
from sanic import response as rs
from sanic.response import json
from sanic.response import text
from sanic.response import stream
from sanic.views import HTTPMethodView
from sanic.views import stream as stream_decorator
from sanic.exceptions import abort
from sanic_cors import CORS

from sanic.blueprints import Blueprint

import motw
import asyncio
import uvloop


proc = psutil.Process(os.getpid())

if len(sys.argv) > 2:
    setproctitle(sys.argv[2])
else:
    setproctitle("sanic_motw")


app = Sanic()
CORS(app)
bp = Blueprint('blueprint_request_stream')
app.config.REQUEST_MAX_SIZE = 1000000000

# - initial (re)load
if not pathlib.Path("unitedstates").exists():
    pathlib.Path("unitedstates").mkdir(parents=True, exist_ok=True)


def check_library_secret(library_uuid, library_secret):
    secret = motw.load_libraries()[library_uuid]["secret"]
    ret = (
        True
        if (library_secret == motw.master_secret or library_secret == secret)
        else False
    )
    if ret:
        return ret
    else:
        abort(403)


@sync_to_async
def validate_books(bookson, schema, enc_zlib):
    if enc_zlib:
        try:
            bookson = zlib.decompress(bookson).decode("utf-8")
        except zlib.error as e:
            abort(422, "Unzipping JSON failed...")

    validate = rjson.Validator(rjson.dumps(schema))
    try:
        validate(bookson)
        return bookson
    except ValueError as e:
        print(e)
        abort(422, "JSON didn't validate.")


@sync_to_async
def remove_books(rookson, library_uuid):
    pickled_books = sortedcontainers.SortedDict()
    # bookids = rjson.loads(rookson)
    bookids = ujson.loads(rookson)
    if bookids == []:
        return True
    # t = time.time()
    with (open("unitedstates/{}".format(library_uuid), "rb")) as f:
        pickled_books.update(pickle.load(f))

    for bookid in bookids:
        pickled_books.pop(bookid, None)

    with open("unitedstates/{}".format(library_uuid), "wb") as f:
        pickle.dump(pickled_books, f)

    # print("books removed in {} seconds.".format(round(time.time() - t, 3)))
    return True


@sync_to_async
def add_books(bookson, library_uuid):
    pickled_books = sortedcontainers.SortedDict()
    t = time.time()
    # books = rjson.loads(bookson, datetime_mode=rjson.DM_ISO8601)
    books = ujson.loads(bookson)
    print("books.json loaded in {} seconds.".format(round(time.time() - t, 3)))
    if books == []:
        return True
    library_uuid_check = list(set([book["library_uuid"] for book in books]))
    if len(library_uuid_check) != 1 or library_uuid_check[0] != library_uuid:
        return False

    new_book_ids = set((book["_id"] for book in books))
    t = time.time()

    if not pathlib.Path("unitedstates/{}".format(library_uuid)).exists():
        with open("unitedstates/{}".format(library_uuid), "wb") as f:
            pickle.dump({}, f)

    with (open("unitedstates/{}".format(library_uuid), "rb")) as f:
        pickled_books.update(pickle.load(f))
        old_books_ids = pickled_books.keys()
    print("pickled_books loaded in {} seconds.".format(round(time.time() - t, 3)))

    t = time.time()
    ids_to_add = set(new_book_ids - old_books_ids)
    for b in books:
        if b["_id"] not in ids_to_add:
            continue

        if "series" not in b:
            b["series"] = ""

        pickled_books.update({b["_id"]: b})
    print("pickled_books updated in {} seconds.".format(round(time.time() - t, 3)))

    t = time.time()
    with open("unitedstates/{}".format(library_uuid), "wb") as f:
        pickle.dump(pickled_books, f)
    print("pickled_books dumped in {} seconds.".format(round(time.time() - t, 3)))

    # print("index/url written in {} seconds.".format(round(time.time() - t, 3)))

    del old_books_ids
    del ids_to_add
    del new_book_ids
    del books
    del pickled_books
    del bookson
    gc.collect()
    print(gc.garbage)
    return True


# class Books(HTTPMethodView):
#     def get(self, request, verb, library_uuid):
#         assert request.stream is None
#         return text("Here you should upload, no?")

#     @stream_decorator

@bp.post('/books/<verb>/<library_uuid>', stream=True)
async def bp_handler(request, verb, library_uuid):
    t = time.time()
    print("upload start memory: {}".format(round(proc.memory_full_info().rss / 1000000.0, 2)))
    library_secret = request.headers.get("Library-Secret") or request.headers.get(
        "library-secret"
    )
    encoding_header = request.headers.get(
        "library-encoding"
    ) or request.headers.get("Library-Encoding")
    enc_zlib = False
    if encoding_header == "zlib":
        enc_zlib = True

    check_library_secret(library_uuid, library_secret)

    assert isinstance(request.stream, asyncio.Queue)
    result = b""
    while True:
        body = await request.stream.get()
        if body is None:
            del body
            print("books uploaded in {} seconds.".format(round(time.time() - t, 3)))
            if verb == "add":
                t = time.time()
                bookson = await validate_books(result, motw.collection_schema, enc_zlib)
                print("after validate memory: {}".format(round(proc.memory_full_info().rss / 1000000.0, 2)))
                del result
                print(
                    "books validated in {} seconds.".format(
                        round(time.time() - t, 3)
                    )
                )
                if bookson:
                    t = time.time()
                    if await add_books(bookson, library_uuid):
                        print(
                            "books added in {} seconds.".format(
                                round(time.time() - t, 3)
                            )
                        )
                        print("after add books memory: {}".format(round(proc.memory_full_info().rss / 1000000.0, 2)))
                        del bookson
                        gc.collect()
                        print(gc.garbage)
                        return text("Books added...")
            elif verb == "remove":
                bookson = await validate_books(result, motw.remove_schema, enc_zlib)

                if bookson:
                    if await remove_books(bookson, library_uuid):
                        return text("Books removed...")

            abort(422, "{}-ing books failed!".format(verb))
        result += body

app.blueprint(bp)
# app.add_route(Books.as_view(), "/books/<verb>/<library_uuid>")
app.run(host="0.0.0.0", port=int(sys.argv[1]), workers=1, debug=False, access_log=False)
