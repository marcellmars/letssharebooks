import json
from motw_client import calibre_to_json

DB_PATH = "/home/m/CalibreLibraries/"
OUTPUT_DIRECTORY = "/home/m/CalibreLibraries/libraries_datajs/"

libraries = [
    ("anybody", "192350bd-43d1-45b6-ab39-e9142f28e77a"),
    ("badco", "5268baec-b884-4a75-be84-ed0ca791f465"),
    ("biopolitics", "ee0bdf36-ff42-418b-ac3d-bb983fb96f5a"),
    ("calamitousannunciation", "3693fec5-292d-485e-b93e-b68b1873a3e6"),
    ("brb", "64bbfb1b-0119-44c8-85d6-9675bec3d21c"),
    ("economics", "211c59b5-f60f-47a7-b148-74c0c892397a"),
    ("feminism", "01d1276a-ab48-4d02-bba4-4a5e832432d2"),
    ("herman", "72f60870-2e60-402f-9c4c-2809f5842b0b"),
    ("hortense", "3624df20-00e8-484d-9d7b-41dc2ba89ac3"),
    ("kok", "60134c32-5c64-49ae-96d3-9e96df7572bb"),
    ("marcell", "fac314b6-cfa9-4d2b-99d6-e40a37599cdd"),
    ("midnightnotes", "19eb09e7-16c8-4e17-ba29-0013bc0ccfab"),
    ("otpisane", "5c6c45c2-1357-4c97-bccb-3346b7612aea"),
    ("praxis", "640890f8-eb43-4ea7-9af6-b53f8fe521d0"),
    ("quintus", "227f443c-21ae-416e-838d-7cdd12878155"),
    ("slowrotation", "08fd8cc7-7bef-4174-80eb-074f19c04bed"),
    ("tamoneki", "157c99c3-a6d4-447e-8724-a05fdeba3be7"),
    ("outernationale", "a8ffd2bc-b7bc-4d56-97f4-1457710d432a"),
]

for library in libraries:
    library_uuid = library[1]
    library_secret = library[1]
    librarian = library[0]
    db_path = f"{DB_PATH}{library[0]}/metadata.db"
    print(f"{librarian} rendered with db from {db_path}")
    with open(f"{OUTPUT_DIRECTORY}{library[0]}_data.js", "w") as f:
        f.write(
            "CALIBRE_BOOKS={}".format(
                json.dumps(
                    {
                        "portable": True,
                        "books": calibre_to_json(
                            library_uuid, library_secret, librarian, db_path
                        ),
                    }
                )
            )
        )
