export function dynamicSort(property) {
    var sortOrder = 1;

    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function(a, b) {
        if (sortOrder == -1) {
            return b[property].localeCompare(a[property]);
        } else {
            return a[property].localeCompare(b[property]);
        }
    };
}

export function mockHttpRequest(books, request_url, max_results, title = "books") {
    let url = new URL(request_url);

    let restList = url.pathname.split("/");
    let restVerb = restList[1];
    let restField = restList[2];
    let restQuery = decodeURIComponent(restList[3]);

    if (restVerb == "librarians") {
        if (restField == "on") {
            let librarians = new Set();
            for (let b of books) {
                librarians.add(b.librarian);
            }
            return {
                "_items": [...librarians]
            };
        }
    }

    if (restVerb == "autocomplete") {
        if (restField == "authors") {
            let authors = [];
            for (let b of books) {
                for (let a of b.authors) {
                    if (a.toLowerCase().indexOf(restList[3].toLowerCase()) != -1) {
                        authors.push(a);
                    }
                }
            }
            return {
                "_items": authors
            };
        } else
        if (restField == "tags") {
            let tags = [];
            for (let b of books) {
                for (let t of b.tags) {
                    if (t.toLowerCase().indexOf(restList[3].toLowerCase()) != -1) {
                        tags.push(t);
                    }
                }
            }
            return {
                "_items": tags
            };
        } else
        if (restField == "titles") {
            let titles = [];
            for (let b of books) {
                if (b.title.toLowerCase().indexOf(restList[3].toLowerCase()) != -1) {
                    titles.push(b.title);
                }
            }
            return {
                "_items": titles
            };
        }

    }

    if (restVerb == "search") {
        title = `search in ${restField}`;
        let bz = [];
        for (let b of books) {
            if (["authors", "languages", "tags"].includes(restField)) {
                for (let f of b[restField]) {
                    if (f.toLowerCase().indexOf(restQuery.toLowerCase()) != -1) {
                        bz.push(b);
                    }
                }
            } else
            if (["_id", "title", "titles", "publisher", "series", "library_uuid", "abstract", "librarian"].includes(restField)) {
                if (restField == "titles") {
                    restField = "title";
                }
                if (b[restField].toLowerCase().indexOf(restQuery.toLowerCase()) != -1) {
                    bz.push(b);
                }
            }
        }
        books = bz;
    }

    if (restVerb == "book") {
        for (let b of books) {
            if (b._id == restField) {
                return b;
            }
        }
        if ((CALIBRE_BOOKS1.total - books.length) > 0) {
          return "TRYAGAIN";
        }
    }


    let params = {};
    for (let e of url.searchParams.entries()) {
        params[e[0]] = e[1];
    }
    let p = params.page || 0;

    let lastPage = Math.floor(books.length / max_results + 1);
    let page = Math.max(0, Math.min(lastPage, p - 1));

    let startCut = page * max_results;
    let endCut = (page + 1) * max_results;

    let bookz = [];
    for (let b of books.slice(startCut, endCut)) {
        b["library_url"] = "";
        bookz.push(b);
    }
    let r = {
        "_items": bookz,
        "_links": {
            "parent": {
                "title": "Memory of the World Library",
                "href": "/"
            },
            "self": {
                "title": title,
                "href": "books"
            },
            "prev": {
                "title": "previous page",
                "href": `${url.pathname}?page=${page}`
            },
            "next": {
                "title": "next page",
                "href": `${url.pathname}?page=${page+2}`
            },
            "last": {
                "title": "last page",
                "href": `${url.pathname}?page=${lastPage}`
            },

        },
        "_meta": {
            "page": page + 1,
            "max_results": max_results,
            "total": books.length,
            "status": title
        }
    };
    if (page == 0) {
        delete r._links.prev;
    }
    if (page == lastPage - 1) {
        delete r._links.next;
    }
    return r;
}

export const maxResults = 48;
