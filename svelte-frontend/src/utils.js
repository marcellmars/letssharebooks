function readableBytes(bytes) {
    var i = Math.floor(Math.log(bytes) / Math.log(1024)),
        sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + sizes[i];
}

export function getFormats(book, sep=", ") {
    if (book.formats === undefined || book.formats.length == 0) {
        return [{"url": "", "size":"", "title":""}]
    }

    let f = [];
    for (let frm of book.formats) {
        let url = book.library_url + frm.dir_path + frm.file_name
        let title = frm.format.toUpperCase() + sep;
        f.push({
            url: url,
            title: title,
            size: readableBytes(frm.size)
        });
    }
    f[f.length - 1].title = f[f.length - 1].title.slice(0, -2)
    return f
}

export function showLibrarian() {
    this.showLibrarianName = true;
    setTimeout(() => this.showLibrarianName = false, 2000);
}
