import Vue from 'vue';
import VueRouter from 'vue-router';

import BootstrapVue from 'bootstrap-vue';
// import VueResource from 'vue-resource';
import axios from 'axios';
import {
    store
} from './store/store';

import App from './components/App.vue';
import MotwHeader from './components/MotwHeader.vue';
import LibraryCovers from './components/LibraryCovers.vue';
import LibraryTable from './components/LibraryTable.vue';
import SearchBar from './components/SearchBar.vue';
import LoadingSpinnerModal from './components/LoadingSpinnerModal.vue';

Vue.use(VueRouter);
// Vue.use(VueResource);
Vue.use(BootstrapVue);
// Vue.use(axios);

import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';

Vue.component('motw-header', MotwHeader);
Vue.component('library-covers', LibraryCovers);
Vue.component('library-table', LibraryTable);
Vue.component('search-bar', SearchBar);
Vue.component('loading-spinner-modal', LoadingSpinnerModal);

const base = axios.create({
    // baseURL: 'http://localhost:2018',
    baseURL: '//books.memoryoftheworld.org',
    // baseURL: '//185.193.125.74',
    timeout: 9000
});

Vue.prototype.$http = base;

export const eventBus = new Vue();

import {
    routes
} from './router.js';
export const router = new VueRouter({
    routes
});

new Vue({
    el: "#app",
    store,
    router,
    render: h => h(App)
});
